module Rodauth
  class Oauth
    SCOPES: Array[Symbol]

    SERVER_METADATA: untyped

    # Application

    def oauth_server_metadata: (?String? issuer) -> void

    # /oauth-applications routes
    def oauth_applications: () -> void

    def check_csrf?: () -> bool

    # Overrides logged_in?, so that a valid authorization token also authnenticates a request
    def logged_in?: () -> boolish

    def accepts_json?: () -> boolish

    def scopes: () -> Array[String] 

    def redirect_uri: () -> String?

    def oauth_application: () -> db_record?

    def fetch_access_token: () -> String?

    def authorization_token: () -> String?

    def require_oauth_authorization: (*String scopes) -> void

    def post_configure: () -> void

    def use_date_arithmetic?: () -> true

    private

    def rescue_from_uniqueness_error: () { () -> void } -> void

    def oauth_tokens_unique_columns: () -> Array[Symbol]

    def authorization_server_url: () -> String

    def authorization_server_metadata: () -> [Hash[Symbol, untyped], Integer]

    def introspection_request: (String token_type_hint, String token) -> Hash[String, untyped]

    def before_introspection_request: (untyped request) -> void

    def template_path: (_ToS page) -> String

    def require_oauth_application: () -> void

    def secret_matches?: (db_record oauth_application, String secret) -> untyped

    def secret_hash: (String secret) -> String

    def oauth_unique_id_generator: () -> String

    def generate_token_hash: (String token) -> String

    def token_from_application?: (db_record oauth_token, db_record oauth_application) -> untyped

    def generate_oauth_token: (?Hash[Symbol, untyped] params, ?bool should_generate_refresh_token) -> db_record

    def _generate_oauth_token: (?Hash[Symbol, untyped] params) -> db_record

    def oauth_token_by_token: (String token) -> db_record

    def oauth_token_by_refresh_token: (Strinng token, ?revoked: bool revoked) -> db_record

    def json_access_token_payload: (db_record oauth_token) -> Hash[String, untyped]

    def oauth_application_params: () -> Hash[String, untyped]

    def validate_oauth_application_params: () -> void

    def create_oauth_application: () -> void

    # Authorize
    def require_authorizable_account: () -> void

    def validate_oauth_grant_params: () -> void

    def try_approval_prompt: () -> void

    def create_oauth_grant: (?Hash[Symbol, untyped] create_params) -> String

    def do_authorize: (?Hash[String, untyped] response_params, ?String? response_mode) -> ::Array[untyped]

    def _do_authorize_code: () -> Hash[String, untyped]

    def _do_authorize_token: () -> Hash[String, untyped]

    def validate_oauth_token_params: () -> void

    def create_oauth_token: () -> untyped

    def create_oauth_token_from_authorization_code: (db_record oauth_grant, Hash[Symbol, untyped] create_params) -> db_record

    def create_oauth_token_from_token: (db_record oauth_token, Hash[Symbol, untyped] update_params) -> db_record

    def validate_oauth_introspect_params: () -> void

    def json_token_introspect_payload: (String token) -> Hash[Symbol, untyped]

    def validate_oauth_revoke_params: () -> void

    def revoke_oauth_token: () -> db_record

    def redirect_response_error: (String error_code, ?String redirect_url) -> void

    def json_response_success: (String body, ?bool cache) -> void

    def throw_json_response_error: (Integer status, String error_code) -> void 

    def authorization_required: () -> void

    def check_valid_uri?: (String uri) -> bool

    def check_valid_scopes?: () -> bool

    def check_valid_redirect_uri?: () -> bool

    def check_valid_access_type?: () -> bool

    def check_valid_approval_prompt?: () -> bool

    def check_valid_response_type?: () -> bool

    def validate_pkce_challenge_params: () -> void 

    def check_valid_grant_challenge?: (db_record grant, String verifier) -> untyped

    def oauth_server_metadata_body: (String path) -> Hash[Symbol, untyped]
  end
end
