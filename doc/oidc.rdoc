= Documentation for Open ID Connect feature

The +oidc+ feature builds on top of the oauth_jwt feature to implement an OpenID Connect identity provider.

== Value Methods

use_rp_initiated_logout? :: whether to use RP-initiated logout flow, +false+ my default.

oauth_application_default_scope :: overwrites the default to <tt>"openid"</tt>
oauth_application_scopes :: overwrites the default to <tt>["openid"]</tt>.
oauth_grants_nonce_column :: db column where an authorization nonce is stored, <tt>:nonce</tt> by default.
oauth_tokens_nonce_column :: db column where a token respective nonce is stored, <tt>:nonce</tt> by default.
oauth_applications_post_logout_redirect_uri_column :: db colummn where the logout redirect URI is stored.
oauth_applications_id_token_encrypted_response_alg_column :: db column where to store the encryption algorithm used for the id token for the oauth application, <tt>:id_token_encrypted_response_alg</tt> by default.
oauth_applications_id_token_encrypted_response_enc_column :: db column where to store the encryption method used for the id token for the oauth application, <tt>:id_token_encrypted_response_enc</tt> by default.
oauth_applications_id_token_signed_response_alg_column :: db column where to store the signing algorithm used for the id token for the oauth application, <tt>:id_token_signed_response_alg</tt> by default.
oauth_applications_userinfo_encrypted_response_alg_column :: db column where to store the encryption algorithm used for the userinfo response payload for the oauth application, <tt>:userinfo_encrypted_response_alg</tt> by default.
oauth_applications_userinfo_encrypted_response_enc_column :: db column where to store the encryption method used for the userinfo response payload for the oauth application, <tt>:userinfo_encrypted_response_enc</tt> by default.
oauth_applications_userinfo_signed_response_alg_column :: db column where to store the signing algorithm used for the userinfo response payload for the oauth application, <tt>:userinfo_signed_response_alg</tt> by default.

invalid_scope_message :: overwrites the default to <tt>"The Access Token expired"</tt>
webfinger_relation :: webfinger openid relation filter, <tt>"http://openid.net/specs/connect/1.0/issuer"</tt> by default.

userinfo_route :: the route for the userinfo action, defaults to +userinfo+.
oidc_logout_route :: the route which user uses to logout.

== Auth methods

get_oidc_param :: returns the value for an OpenID connect claim (such as "email", "name", "phone_number", etc...)
get_additional_param :: sets the values for additional scopes.

oauth_prompt_login_cookie_key :: try prompt cookie key.
oauth_prompt_login_cookie_options :: prompt cookie options.
oauth_prompt_login_interval :: prompt cookie lifetime.


before_userinfo_route :: Run arbitrary code before the userinfo route.
before_oidc_logout_route :: Run code before OIDC RP-initiated logging out route.
