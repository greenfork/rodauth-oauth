= Documentation for OAuth Token Introspection feature

The +oauth_token_introspection+ feature implements the OAuth 2.0 Token Introspection.

https://tools.ietf.org/html/rfc7662

== Auth Value Methods

introspect_route :: the route for introspecting access tokens, defaults to +introspect+.
before_introspect_route :: Run arbitrary code before the introspect route.
before_introspect :: Run arbitrary code before introspecting a tokne.
before_introspection_request :: called before introspecting on a given access token (resource server only).