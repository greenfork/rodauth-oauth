# frozen_string_literal: true

require "rodauth"

require "rodauth/oauth/version"

require "rodauth/oauth/railtie" if defined?(Rails)
